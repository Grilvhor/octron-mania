extends Control


onready var label = $Panel/Label

# nakama_leaderboard =
# next_cursor: Null, owner_records: Null, prev_cursor: Null, records: [create_time: 2020-03-09T09:16:26Z, expiry_time: 2020-03-16T00:00:00Z, leaderboard_id: octron-mania-666999, max_num_score: 1000000, metadata: {}, num_score: 1, owner_id: 4c4af2ed-afb2-4f1c-bf95-125090f525b4, rank: 1, score: 59270, subscore: Null, update_time: 2020-03-09T09:16:26Z, username: xhhJpqmkxF, ],

func update_with_nakama_leaderboard(nakama_leaderboard):
	var as_plain_text = ""
	for record in nakama_leaderboard.records:
		var score_string = get_score_string(int(record.score))
		as_plain_text += "%02d %s %s\n" % [int(record.rank), score_string, record.username.substr(0, 3)]
	if not as_plain_text:
		as_plain_text = "No records\nyet\nfor this world\n\n\n(Hide with L)"
	label.set_text(as_plain_text)




#### DUPLICATES ################################################################
## To refactor somewhere else !


func get_score_string(score):
	return get_score_s_string(score) + get_score_ms_string(score)


func get_score_s_string(score):
	if score == INF:
		return "--:--"
	score = int(score / 1000.0)
	return "%02d:%02d" % [score / 60, score % 60]


func get_score_ms_string(score):
	if score == INF:
		return ".--"
	score = int(round(score/10.0)) % 100
	return ".%02d" % [score]
