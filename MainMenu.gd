extends Spatial

onready var play_button = $Control/PlayButton

func _ready():
	print('Readying Main Menu…')
	play_button.grab_focus()
	play_button.connect("pressed", self, "start_playing")

func start_playing():
	show_loading_screen()
	# …
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	god.go_to_current_world()

func show_loading_screen():
	$LoadingScene.set_visible(true)
