extends AudioStreamPlayer

#Music player SINGLETON, handle music playlist.
#Stage music is randomly determined
#Main menu music is only loaded at the creation of node.


#Loading all OGG files from this directory
var music_path_stage = "res://assets/music/stage"

# Called when the node enters the scene tree for the first time.
func _ready():
	
	create_playlist_from_dir()
	
	
var playlist:Array = Array()
func create_playlist_from_dir():
	
	var dir = Directory.new()
	if dir.open(music_path_stage) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
#				print("Found directory: " + file_name)
				pass
			else:
#				print("Found file: " + file_name)
				if file_name.ends_with(".ogg"):
					
					playlist.append({"name":file_name,"path":"%s/%s" %[music_path_stage,file_name]})
				else:
					pass
#					print("Not OGG file.")
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")

#	print("playlist", playlist)
	

func get_track():
	
	randomize()
	var num = randi() % playlist.size()
	print("loading %s; track number %d" % [playlist[num].name, num])
	set_stream(load(playlist[num].path))
