extends Node


const GameState = preload("res://OctronManiaGameState.gd")
const WorldScene = preload("res://OctronWorld.tscn")
const ScoreManager = preload("res://ScoreManager.gd")

var game_state:GameState
var score_manager:ScoreManager

func _init():
	if not self.game_state:
		self.game_state = GameState.new()
	score_manager = ScoreManager.new(true)


func _input(event):
	if Input.is_action_just_pressed("toggle_fullscreen"):
		OS.window_fullscreen = not OS.window_fullscreen


func go_to_current_world():
	return go_to_world(self.game_state.get_current_world_id())


func go_to_next_world():
	return go_to_world(self.game_state.get_next_world_id())


func go_to_world(world_id):
	var world = WorldScene.instance()
	world.world_id = world_id
	var current_world = get_tree().current_scene
	
	if current_world:
		get_node("/root").remove_child(current_world)
		current_world.queue_free()
	
	get_node("/root").add_child(world)
	get_tree().current_scene = world


# ---


const ConfigFiles = preload("res://addons/goutte.configfiles/config_files.gd")
var nakama_client:NakamaClient
var nakama_session:NakamaSession

func get_nakama_client() -> NakamaClient:
	if self.nakama_client:
		return self.nakama_client
	
	var secrets = ConfigFiles.load_from_files([
		'res://secrets.json.dist',
		'res://secrets.json',  # create your own!
	])
	
	print("Creating Nakama client…")
	self.nakama_client = Nakama.create_client(
		secrets['server_key'],
		secrets['host'],
		secrets['port'],
		secrets['scheme']
	)
	
	return self.nakama_client


func get_nakama_session() -> NakamaSession:
	if not self.nakama_session:
		
		if not self.nakama_client:
			self.nakama_client = get_nakama_client()
		assert(self.nakama_client)
		
		print("Creating Nakama session…")
		var deviceid = OS.get_unique_id() # This is not supported by Godot in HTML5, use a different way to generate an id, or a different authentication option.
		self.nakama_session = yield(self.nakama_client.authenticate_device_async(deviceid), "completed")
	#	if not nakama_session.is_exception():
	#		cfg.set_value(STORE_SECTION, STORE_KEY, session.token)
	#		cfg.save(STORE_FILE)
		print("Nakama session created:")
		print(self.nakama_session._to_string())
		print("With this information, you CAN cheat!  Please don't. :)")
		print("\"Your scientists were so preoccupied with whether or not they could, they didn’t stop to think if they should.\"")
		print("    — Ian, Jurassic Park")
	
	return self.nakama_session


