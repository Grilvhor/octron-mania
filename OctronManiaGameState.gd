extends Resource

#export var worlds_ids:Array
export var worlds:Array
export var current_world_index:int = 0

func _init():
	if worlds.empty():
		# … load from file? todo
		# … meanwhile …
		worlds = [
			{
				'id': 666999,
				'title': "Devil's Equation",
			},
			{
				'id': 999666,
				'title': "Strolling Hill",
			},
			{
				'id': 987654,
				'title': "The Pirate Bay",
			},
			{
				'id': 654321,
				'title': "The Coral",
			},
			{
				'id': 1,
				'title': "The Octocave",
			},
			{
				'id': 7770777,
				'title': "Lucky Shenanigans",
			},
			{
				'id': 444,
				'title': "Get dat Parkour",
			},
			{
				'id': 456789,
				'title': "Vulture",
			},
			{
				'id': 5551555,
				'title': "Abyss",
			},
			{
				'id': 748596,
				'title': "The Ark",
			},
			{
				'id': 7,
				'title': "The Rock",
			},
			{
				'id': 123456,
				'title': "The Lion King",
			},
			{
				'id': 43,
				'title': "The Tower",
			},
#			{
#				'id': 51,
#				'title': "lag",
#			},
			
#			{
#				'id': 3,
#				'title': "Boring",
#			},
			
#			{
#				'id': 1006,
#				'title': "Edgy Living",
#			},
		]
#		worlds_ids = [
#			666999,
#			999666,
#			666999818,
#			43,
#		]
		# L∞p
#		worlds_ids.append(worlds_ids.front())

func get_current_world():
	return self.worlds[self.current_world_index]

func get_current_world_id():
	return get_current_world()['id']

func get_next_world():
	self.current_world_index = (self.current_world_index+1) % self.worlds.size()
	return get_current_world()

func get_next_world_id():
	return get_next_world()['id']
#	self.current_world_id = self.worlds_ids[(
#		self.worlds_ids.find(self.current_world_id) + 1
#		) % self.worlds_ids.size()]
#	return self.current_world_id
