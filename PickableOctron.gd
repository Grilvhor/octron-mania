extends Spatial

# Always make sure you set the position of this node
# BEFORE you add it to the scene.

enum STATE {
	FLOATING,
	FOLLOWING,
	EXPANDING,
	ABSORBING,
}

export(Color) var color:Color = Color("#d5cb37")
export var oscillation_speed = 1.6
export var oscillation_amplitude = 0.5

export var state = STATE.FLOATING

var target_position = Vector3()
var time = 0

func _ready():
	target_position = translation
	time += rand_range(0, TAU)
	for i in range(14):
#		var mat = $MeshInstance.mesh.surface_get_material(i)
		var mat = SpatialMaterial.new()
		mat.albedo_color = self.color
		mat.emission = self.color
		mat.emission_enabled = true
#		mat.emission_energy = 0.42
		$MeshInstance.mesh.surface_set_material(i, mat)
	$OmniLight.light_color = self.color

var velocity:Vector3 = Vector3.ZERO # only used when following
func _process(delta):
	time += delta
	if self.state == STATE.FLOATING:
		var delta_y = oscillation_amplitude * sin(time * oscillation_speed)
		self.translation = target_position + Vector3(0, delta_y, 0)
	elif self.state == STATE.FOLLOWING:
		var orbit_target = self.target_node.get_orbit_of(self)
		var to_target = orbit_target - self.translation
		if to_target.length_squared() > 1:
			to_target = to_target.normalized()
		self.velocity += to_target * 0.017
		self.translation += self.velocity
		self.velocity *= 0.95
	elif self.state == STATE.EXPANDING:
		var orbit_target = self.target_node.get_expand_of(self)
		var to_target = orbit_target - target_position
		target_position += to_target * 0.09
		self.translation = target_position
	elif self.state == STATE.ABSORBING:
		var orbit_target = self.target_node.to_global(Vector3())
		var to_target = orbit_target - target_position
		target_position += to_target * 0.125
		self.translation = target_position
	else:
		assert(false)

var target_node
func follow(that_node):
	self.target_node = that_node
	self.state = STATE.FOLLOWING

func is_following():
	return null != self.target_node

func animate_pickup():
	$AnimationPlayer.play("picked_up")

var pre_absorbing = false
var absorbing = false
func animate_absorb():
	var i = target_node.orbitals[self]
	yield(get_tree().create_timer(0.2 * sqrt(i*2) + 0.15), "timeout")
	if $AnimationPlayer.is_playing():
		yield($AnimationPlayer, "animation_finished")
	$AnimationPlayer.play("absorbed", -1, 0.222)

func start_expanding():
	target_position = translation
	state = STATE.EXPANDING

func start_absorbing():
	target_position = translation
	state = STATE.ABSORBING

func colorize_target():
	target_node.colorize_surface_for(self, self.color)
	target_node.play_absorption_sound(self)

func destroy():
	if target_node:
		target_node.remove_orbital(self)
	queue_free()
