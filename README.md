# Octron Mania

![A terrain made of Octrons with yellow, floating octrons.](promo/screenshot_00.png)

A time-attack game made with Godot 3.2 and Truncated Octahedrons, aka Octrons.

You can also go into "editor mode" and add and remove octrons with the mouse. (press F)
