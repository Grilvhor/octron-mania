extends "res://addons/goutte.camera.trackball/trackball_camera.gd" 

func ready():
	capture_mouse()
	self.stabilize_horizon = true
	self.mouse_move_mode = true
	self.action_invert = true
	self.action_zoom_in = "cam_zoom_in"
	self.action_zoom_out = "cam_zoom_out"
	.ready()

func capture_mouse():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func liberate_mouse():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
func toggle_mouse_mode():
	if self.mouse_move_mode:
		liberate_mouse()
		self.mouse_move_mode = false
		self.action_invert = false
	else:
		capture_mouse()
		self.mouse_move_mode = true
		self.action_invert = true
