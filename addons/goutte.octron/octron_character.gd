#extends KinematicBody
# It's just immediately more fun with the physics of
extends RigidBody


#const OctronMesh = preload("res://addons/goutte.octron/octron_mesh.gd")
const OctronLattice = preload("res://addons/goutte.octron/octron_lattice.gd")


export var jump_strength = 10.0  # TODO
export var jump_cooldown = 2222  # ms
export var orbit_radius = 1.7
export var orbitals_speed = 0.333  # turns/s


func _ready():
#	generate_collision_shape()
#	generate_mesh()
	generate_eye()
	
	var pm = PhysicsMaterial.new()
	pm.bounce = 0.0
	pm.friction = 0.555
	self.physics_material_override = pm


onready var shape = $CollisionShape
#var shape
#func generate_collision_shape():
#	self.shape = CollisionShape.new()
#	self.shape.shape = preload("res://addons/goutte.octron/geo/octron.shape")
#	add_child(self.shape)


onready var mesh = $MeshInstance
#var mesh
#func generate_mesh():
#	self.mesh = MeshInstance.new()
#	self.mesh.mesh = OctronMesh.new()
#	add_child(self.mesh)


# Eye to use as anchor for the camera.
# It follows the character but does not rotate with it.
# TODO: delete it as well when this character is deleted.
var eye
func generate_eye():
	self.eye = Spatial.new()
	self.eye.name = "%sEye" % name
	get_parent().add_child(self.eye)


func get_eye_position():
	var destination = self.to_global(Vector3(0, 0, 0))
	destination += Vector3(0, 1, 0)
	return destination


var orbitals = Dictionary()
#var orbitals = Array()  # hmmm…  which is faster for us?

func add_orbital(spatial_node):
	assert(not orbitals.has(spatial_node))
	self.orbitals[spatial_node] = self.orbitals.size()
	
	#colorize_surface_for(spatial_node, ColorN('chartreuse'))


func remove_orbital(orbital):
	self.orbitals.erase(orbital)


func get_orbit_of(orbital):
	var time = (OS.get_ticks_msec() * 0.001) * orbitals_speed * TAU
	var os = self.orbitals.size()
	var theta = self.orbitals[orbital] * TAU / os
	var curve = Vector3(sin(theta + time), orbital.oscillation_amplitude * sin(theta + time * orbital.oscillation_speed), cos(theta + time))
	return to_global(Vector3()) + orbit_radius * (1 + 0.22*sin(time)) * curve


func get_expand_of(orbital):
	var direction = OctronLattice.DIRECTIONS[self.orbitals[orbital]]
	return to_global(direction)


func colorize_surface_for(orbital, color):
	var i = self.orbitals[orbital]
#	color = ColorN('aquamarine')
	var colorized_material = SpatialMaterial.new()
	colorized_material.set_albedo(color)
	colorized_material.set_emission(color)
	colorized_material.emission_enabled = true
	self.mesh.set_surface_material(i, colorized_material)


func play_absorption_sound(orbital):
	var i = self.orbitals[orbital]
	var player:AudioStreamPlayer = get_node("AudioStreamPlayer%02d" % [randi() % 3])
	#player.stream.loop = false
	player.pitch_scale = 0.2 + 0.9 * (i / 14.0)
	player.play()
#	player.connect("finished", self, "stop_absorption_sound", [player])


#func stop_absorption_sound(player):
#	print('FINISHED')
#	player.stop()


func _process(delta):
	var destination = get_eye_position()
	self.eye.translation = destination
