extends Resource

# A lattice of truncated octahedrons.
# Can hold at most `stack_size` things per cell.
# Starts empty.
# Cells are Vector3, with integers.
# Each cell has 14 neighbors.

enum HUMAN_DIRECTIONS {
	# 6 squares
	FRONT,
	BACK,
	TOP,
	BOTTOM,
	RIGHT,
	LEFT,
	# 8 Hexagons
	FRONT_TOP_LEFT,
	FRONT_BOTTOM_LEFT,
	FRONT_BOTTOM_RIGHT,
	FRONT_TOP_RIGHT,
	BACK_TOP_RIGHT,
	BACK_BOTTOM_RIGHT,
	BACK_BOTTOM_LEFT,
	BACK_TOP_LEFT,
}

# In cartesian space, offsets between adjacent cells
# Thoe order of the directions is the order of the surfaces
# in the Octron mesh.
const DIRECTIONS = [
	# Squares
	Vector3(  0,  0,  2 ),
	Vector3(  0,  0, -2 ),
	Vector3(  0,  2,  0 ),
	Vector3(  0, -2,  0 ),
	Vector3(  2,  0,  0 ),
	Vector3( -2,  0,  0 ),
	# Hexagons
	Vector3( -1,  1,  1 ),
	Vector3( -1, -1,  1 ),
	Vector3(  1, -1,  1 ),
	Vector3(  1,  1,  1 ),
	Vector3(  1,  1, -1 ),
	Vector3(  1, -1, -1 ),
	Vector3( -1, -1, -1 ),
	Vector3( -1,  1, -1 ),
]

# At least two ways to model this:
# - BCC = Body-Centered Cubic
# - XYZ = Cartesian

# Maximum amount of things allowed per cell
export(int, 1, 999) var stack_size = 1

# Cartesian Vector3 => Array of mixed
var map = Dictionary()

###

func _create_array_if_needed(on_cell):
	if not map.has(on_cell):
		map[on_cell] = Array()

func can_add_thing(on_cell):
	if not map.has(on_cell):
		return true
	assert(map[on_cell] is Array)  # te remove later
	return map[on_cell].size() < stack_size

func add_thing(thing, on_cell):
	_create_array_if_needed(on_cell)
	if can_add_thing(on_cell):
		map[on_cell].append(thing)

func remove_thing(thing, on_cell):
	assert(map.has(on_cell))
	var thing_position = map[on_cell].find(thing)
	assert(thing_position > -1)
	map[on_cell].remove(thing_position)
	if map[on_cell].empty():
		map.erase(on_cell)

func remove_things(on_cell):
	if not map.has(on_cell):
		return
	assert(map[on_cell] is Array)
	map[on_cell].clear()
	map.erase(on_cell)

###

func get_all_cells():
	return map.keys()

func get_random_cell(rng):
	var cells = map.keys()
	assert(0 < cells.size())
	return cells[rng.randi_range(0, cells.size() - 1)]

###

func has_thing_on(cell):
	return self.map.has(cell)

func has_14_neighbors(cell):
	var it_does = true
	for direction in DIRECTIONS:
		if not has_thing_on(cell + direction):
			it_does = false
			break
	return it_does

###

func to_up():
	return DIRECTIONS[HUMAN_DIRECTIONS.TOP]

func move_node_to_cell(node, cell):
	node.set_translation(cell)

###

func is_cell_valid(cell):
	return is_xyz_valid(cell.x, cell.y, cell.z)

func is_xyz_valid(x, y, z):
	"""
	Not all (x, y, z) cartesian triplets are 'valid' cells.
	Since an octron lattice is similar to a cubic lattice
	with one cube every two, like black tiles in chess, but in 3D,
	we can use that to check if the coordinates match.
	"""
	assert(x is int)
	assert(y is int)
	assert(z is int)
	var ax2 = abs(x % 2)
	var ay2 = abs(y % 2)
	var az2 = abs(z % 2)
	return (ax2 == az2) and (ay2 == az2)

