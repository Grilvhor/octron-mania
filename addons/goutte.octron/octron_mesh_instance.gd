extends MeshInstance

# 
# Octron = Truncated Octahedron = Mecon
# 

var OctronMesh = preload("res://addons/goutte.octron/octron_mesh.gd")

export(Texture) var square_texture:Texture
export(Texture) var hexagon_texture:Texture

func _ready():
	if not mesh:
		build_mesh()


func build_mesh():
	"""
	(Re)build the mesh of this mesh instance.
	"""
	#print("[%s] Building Octron mesh…" % [name])
	mesh = OctronMesh.new()
	if self.square_texture:
		var square_material = SpatialMaterial.new()
		square_material.set_texture(SpatialMaterial.TEXTURE_ALBEDO, self.square_texture)
		for i_surf in range(0, 6):
			mesh.surface_set_material(i_surf, square_material)
	if self.hexagon_texture:
		var hexagon_material = SpatialMaterial.new()
		hexagon_material.set_texture(SpatialMaterial.TEXTURE_ALBEDO, self.hexagon_texture)
		for i_surf in range(6, 14):
			mesh.surface_set_material(i_surf, hexagon_material)
	#ResourceSaver.save("res://addons/goutte.octron/octron.mesh", mesh)
	#mesh.export_shape("res://addons/goutte.octron/octron.shape")
	#mesh.export_shape("res://addons/goutte.octron/octron_without_margins.shape", true)
