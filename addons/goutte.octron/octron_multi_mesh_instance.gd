extends MultiMeshInstance

var TruncatedOctahedronMesh = preload("res://addons/goutte.octron/octron_mesh.gd")

func _ready():
	multimesh = MultiMesh.new()
	multimesh.transform_format = MultiMesh.TRANSFORM_3D
	multimesh.color_format = MultiMesh.COLOR_NONE
	multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE
	# Then resize (otherwise, changing the format is not allowed).
	multimesh.instance_count = 20000
	# Maybe not all of them should be visible at first.
	multimesh.visible_instance_count = -1
	# This mesh is not optimized for speed, but it's okay.
	multimesh.mesh = TruncatedOctahedronMesh.new()
	
#	multimesh.mesh.export_shape("OctronShape.shape")

#	var hexagons_material = ShaderMaterial.new()
#	hexagons_material.shader = preload("res://octatron.shader")
#	hexagons_material.set_shader_param("my_texture", preload("res://assets/sprites/red-hex.png"))
#
#	var squares_material = ShaderMaterial.new()
#	squares_material.shader = preload("res://octatron.shader")
#	squares_material.set_shader_param("my_texture", preload("res://assets/sprites/red.png"))
#
#	var mesh_toc = MeshInstance.new()
#	mesh_toc.cast_shadow = true
#	mesh_toc.set_mesh(TruncatedOctahedron.new())
#
#	for i in range(0, 6): # first six are the squares, obviously
#		mesh_toc.set_surface_material(i, squares_material)
#	for i in range(6, 14):
#		mesh_toc.set_surface_material(i, hexagons_material)

var _cell_to_index = Dictionary()
var _index_to_cell = Dictionary()
var _instances_added = 0
func add_octron(where=Vector3()):
	assert(multimesh)  # this node is not _ready yet
	#print("Adding octron at %s." % where)
	multimesh.set_instance_transform(
		_instances_added,
		Transform(Basis(), where)
	)
	_cell_to_index[where] = _instances_added
	_index_to_cell[_instances_added] = where
	
	_instances_added += 1
	multimesh.visible_instance_count = _instances_added


func remove_octron(on_cell):
	print("Removing octron on cell %s, at multimesh index %d." % [
		on_cell,
		_cell_to_index[on_cell],
	])
	assert(_instances_added > 0) # perhaps skip silently, eventually?
	var last_index = _instances_added - 1
	var tmp_tr = multimesh.get_instance_transform(last_index)
	var tmp_cell = _index_to_cell[last_index]
	
	multimesh.set_instance_transform(_cell_to_index[on_cell], tmp_tr)
	_index_to_cell[_cell_to_index[on_cell]] = tmp_cell
	_cell_to_index[tmp_cell] = _cell_to_index[on_cell]
	_index_to_cell.erase(last_index)
	_cell_to_index.erase(on_cell)
	
	_instances_added -= 1
	multimesh.visible_instance_count = _instances_added







