tool
extends EditorPlugin

func _enter_tree():
	add_custom_type(
		"OctronMeshInstance", "MeshInstance",
		preload("res://addons/goutte.octron/octron_mesh_instance.gd"),
		preload("res://addons/goutte.octron/icon_16.png")
	)
	add_custom_type(
		"OctronMultiMeshInstance", "MultiMeshInstance",
		preload("res://addons/goutte.octron/octron_multi_mesh_instance.gd"),
		preload("res://addons/goutte.octron/icon_16.png")
	)

func _exit_tree():
	remove_custom_type("OctronMeshInstance")
	remove_custom_type("OctronMultiMeshInstance")
