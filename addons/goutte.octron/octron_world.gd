extends Spatial

# More of a demo and blackboard for glue code than a reusable script.

# Although we're trying to ensure that methods may be overriden.

# Usage
# extends "res://addons/goutte.octron/octron_world.gd"

# WIP
# Handles
# - a Lattice
# - Character / Camera / Controls ?
# - MultiMeshe(s) for rendering the Octrons (using chunking, probably)


const Octron = preload("res://addons/goutte.octron/octron.gd")
const OctronCamera = preload("res://addons/goutte.octron/octron.camera.gd")
const OctronLattice = preload("res://addons/goutte.octron/octron_lattice.gd")
const OctronCharacter = preload("res://addons/goutte.octron/octron_character.gd")
const OctronMeshInstance = preload("res://addons/goutte.octron/octron_mesh_instance.gd")
const OctronMultiMeshInstance = preload("res://addons/goutte.octron/octron_multi_mesh_instance.gd")

# From anoother plugin ; we'll make our own eventually
#var TrackballCamera = preload("res://addons/goutte.camera.trackball/trackball_camera.gd")


export(String, FILE, "*.tscn,*.scn") var character_scene_path = "res://addons/goutte.octron/octron_character.tscn"


var lattice = OctronLattice.new()
var should_process_inputs = false


func _init():
	init()

func init():
	
#	return
#	read_configuration()
#	connect_to_network()
	configure_lattice()


var is_ready = false
func _ready():
#	emit_signal("scene_ready")
#	return
	
	ready()

signal scene_ready # old test, not used

func ready():
	setup_characters()
	setup_camera()
	setup_controls()
	setup_chunker()
	generate_world()
	yield(get_tree(), "idle_frame")
	after_world_generation()
	enable_inputs()
	is_ready = true  # remove me and test
	emit_signal("scene_ready")

func enable_inputs():
	self.should_process_inputs = true


# Override this method in your own script extending this one
func configure_lattice():
	pass


var characters = Array()  # of OctronCharacters
var character  # currently controlled OctronCharacter
# Override this method in your own script extending this one
func setup_characters():
	if self.character:
		return
	var character_scene = load(self.character_scene_path)
	self.character = character_scene.instance()
	assert(self.character is OctronCharacter)  # we'll need its methods
#	self.character = OctronCharacter.new()
	self.character.can_sleep = false
	#self.character.name = "CharacterOctron"
	self.character.translate(Vector3(0,3,0))
	self.characters.append(self.character)
	add_child(self.character)
	

var camera  # probably an OctronCamera
# Override this method in your own script extending this one
func setup_camera():
	if self.camera:
		return
	self.camera = OctronCamera.new()
	self.camera.translate(Vector3(0,0,9))
	self.camera.set_current(true)
	
	if self.character:
		self.character.eye.add_child(self.camera)
	else:
		add_child(self.camera)

var controls
# Override this method in your own script extending this one
func setup_controls():
	pass


var multimesh  # ← WIP, use chunks
func setup_chunker():
	self.multimesh = OctronMultiMeshInstance.new()
	add_child(multimesh)


# Override this method in your own script extending this one
func generate_world():
	pass


# Override this method in your own script extending this one
func after_world_generation():
	pass


func add_octron(where):
	if lattice.can_add_thing(where):
		self.multimesh.add_octron(where)
		add_octron_node(where)
		var octron = Octron.new()
		lattice.add_thing(octron, where)
	else:
		print("Another octron is in the way.")


var ray_length = 100

func find_octron(cursor_pos):
	var from = camera.project_ray_origin(cursor_pos)
	var to = from + camera.project_ray_normal(cursor_pos) * ray_length
	var space_state = get_world().direct_space_state
	var result = space_state.intersect_ray(from, to)
	#print("Find octron ", result)
	if result and result.has('collider'):
		if _collider_to_cell.has(result['collider']):
			result['cell'] = _collider_to_cell[result['collider']]
	return result


func find_ghost_cell_under_mouse(mouse_pos):
	var octron = find_octron(mouse_pos)
	if not octron:
		return null
	var found_direction = null
	var normal = octron['normal']
	for i in range(OctronLattice.DIRECTIONS.size()):
		var direction = OctronLattice.DIRECTIONS[i]
		if (direction.normalized() - normal).length_squared() < 0.01:
			found_direction = i
			break
	if null == found_direction:
		printerr("Direction not found : %s." % [normal])
		return null
	if not octron.has('cell'):
		printerr("Found collider is not an octron : %s." % [octron])
		return null
		
	return octron['cell'] + OctronLattice.DIRECTIONS[found_direction]


func remove_octron(on_cell):
	multimesh.remove_octron(on_cell)
	lattice.remove_things(on_cell)
	if _cell_to_collider.has(on_cell):
		_collider_to_cell.erase(_cell_to_collider[on_cell])
		_cell_to_collider[on_cell].queue_free()
		_cell_to_collider.erase(on_cell)
	else: # weak – perhaps be silent once we're stable?
		printerr("No octron to remove on cell %s ?" % [on_cell])


var _cell_to_collider = Dictionary()
var _collider_to_cell = Dictionary()
func add_octron_node(where):
#func add_octron_collision_shape(on_cell): ?
	var sb = StaticBody.new()
	_collider_to_cell[sb] = where
	_cell_to_collider[where] = sb
	sb.translate(where)
	# No idea on how Godot handles the following internally… Color picking?
	#sb.connect("mouse_entered", self, "_on_mouse_entered_octron", [where])
	var cs = CollisionShape.new()
	cs.shape = preload("res://addons/goutte.octron/octron.shape")
	sb.add_child(cs)
	add_child(sb)


################################################################################
## TODO: Move to OctronCharacter and signals ###################################

# Clicks dragging further than sqrt(treshold) are ignored here
var _mouse_drift_treshold = 4
var _mouse_left_pressed_pos = Vector2()
var _mouse_right_pressed_pos = Vector2()
func _input(event):
	if not is_ready:
		return
	process_terraforming_inputs(event)
	process_other_inputs(event)

func process_terraforming_inputs(event):
	if (event is InputEventMouseButton) and (event.button_index == BUTTON_LEFT):
		var mouse_pos = get_viewport().get_mouse_position()
		if not event.pressed:
			if (mouse_pos - _mouse_left_pressed_pos).length_squared() < 5:
				var ghost_cell = find_ghost_cell_under_mouse(mouse_pos)
				if null != ghost_cell:
					add_octron(ghost_cell)
		_mouse_left_pressed_pos = mouse_pos
	if (event is InputEventMouseButton) and (event.button_index == BUTTON_RIGHT):
		var mouse_pos = get_viewport().get_mouse_position()
		if not event.pressed:
			if (mouse_pos - _mouse_right_pressed_pos).length_squared() < 5:
				var octron = find_octron(mouse_pos)
				if octron and octron.has('cell'):
					assert(octron.has('cell'))  # or in the if?
					var on_cell = octron['cell']
					remove_octron(on_cell)
		_mouse_right_pressed_pos = mouse_pos

func process_other_inputs(event):
	pass

func _process(delta):
	if not is_ready:
		return
	process(delta)

func process(delta):
	pass

func _physics_process(delta):
	if not is_ready:
		return
	physics_process(delta)

func physics_process(delta):
	if should_process_inputs:
		process_movement_inputs(delta)

func clear_movement_inputs():
#	self.character.linear_velocity *= 0.01
#	self.character.angular_velocity *= 0.01
	self.character.linear_velocity = Vector3.ZERO
	self.character.angular_velocity = Vector3.ZERO

var _last_jumped_at = 0  # s
func process_movement_inputs(delta):
	var action_strength = 15
	var torque_strength = 13
	var velocity_threshold = 11
	var torque_threshold = 10
	# Projecting on the plane allows us to use the camera as referential
	# for controls but using only its "longitude" and ignoring its "latitude"
	# and "altitude". We don't want controls to be less effective when the camera
	# looks at the character slightly from above or below.
	var plane = Plane(Vector3.UP, 0)
	var cam_basis = self.camera.global_transform.basis
	var z = plane.project(cam_basis.z).normalized()
	var x = plane.project(cam_basis.x).normalized()
	# Short variables are sweet
	var vel = self.character.linear_velocity
	var ang = self.character.angular_velocity
	# Variables we're going to fill and use
	var velocity = Vector3.ZERO  # in world space
	var torque = Vector3.ZERO  # in world space
	var direction = Vector3.ZERO  # in world space
	var axis = Vector3.ZERO  # in world space
	# Learn to use dot()
	# Here we use it to see if how much of the current velocity is aligned
	# with our direction vector, and if it's too much, we ignore the input.
	# The dot() of two vectors is highest when vectors are aligned
	# and lowest (in the negatives) when the vectors are opposite.
	# it is 0 when vectors are perpendicular (orthogonal), which is useful too.
	if Input.is_action_pressed("move_forward"):
		direction = z * -1
		if vel.dot(direction) < velocity_threshold:
			velocity += direction
		axis = x * -1
		if ang.dot(axis) < torque_threshold:
			torque += axis
	if Input.is_action_pressed("move_backward"):
		direction = z
		if vel.dot(direction) < velocity_threshold:
			velocity += direction
		axis = x
		if ang.dot(axis) < torque_threshold:
			torque += axis
	if Input.is_action_pressed("move_left"):
		direction = x * -1
		if vel.dot(direction) < velocity_threshold:
			velocity += direction
		axis = z
		if ang.dot(axis) < torque_threshold:
			torque += axis
	if Input.is_action_pressed("move_right"):
		direction = x
		if vel.dot(direction) < velocity_threshold:
			velocity += direction
		axis = z * -1
		if ang.dot(axis) < torque_threshold:
			torque += axis
	velocity = velocity.normalized() * action_strength * delta * 60
	torque = torque.normalized() * torque_strength * delta * 60

	self.character.add_central_force(velocity)
	self.character.add_torque(torque)
	
	var now = OS.get_ticks_msec()
	if Input.is_action_just_pressed("char_jump"):
		if (now - _last_jumped_at) > self.character.jump_cooldown:
			self.character.apply_central_impulse(10 * Vector3.UP)
			_last_jumped_at = now
#			velocity += z * action_strength * -1
################################################################################
################################################################################

