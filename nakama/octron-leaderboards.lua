local nk = require("nakama")

function printf(...) print(string.format(...)) end
    
function table_print (tt, indent, done)
  done = done or {}
  indent = indent or 0
  if type(tt) == "table" then
    local sb = {}
    for key, value in pairs (tt) do
      table.insert(sb, string.rep (" ", indent)) -- indent it
      if type (value) == "table" and not done [value] then
        done [value] = true
        table.insert(sb, key .. " = {\n");
        table.insert(sb, table_print (value, indent + 2, done))
        table.insert(sb, string.rep (" ", indent)) -- indent it
        table.insert(sb, "}\n");
      elseif "number" == type(key) then
        table.insert(sb, string.format("\"%s\"\n", tostring(value)))
      else
        table.insert(sb, string.format(
            "%s = \"%s\"\n", tostring (key), tostring(value)))
       end
    end
    return table.concat(sb)
  else
    return tt .. "\n"
  end
end

function to_string( tbl )
    if  "nil"       == type( tbl ) then
        return tostring(nil)
    elseif  "table" == type( tbl ) then
        return table_print(tbl)
    elseif  "string" == type( tbl ) then
        return tbl
    else
        return tostring(tbl)
    end
end

-------------------------------------------------------------------

local function create_leaderboard_if_inexistent(context, payload)
    --print("create_leaderboard_if_inexistent()")
    local user = nk.users_get_id({context.user_id})[1]
    -- Let's assume we've stored a user's level in their metadata.
    --if user.metadata.level <= 10 then
    --  error("Must reach level 10 before you can add friends.")
    --end

    -- we'll assume payload was sent as JSON and decode it.
    --local payload_json = nk.json_decode(payload)
    
    -- nk.logger_info() does not print in the docker logs somehow.
    -- The server config.yml is setting debug level, though. Weird.
    nk.logger_info("NK LOGGER INFO!!! IF YOU SEE IT WORKS!")
    
    --print(("Create Leaderboard Payload: %q"):format(payload))
    --print(table_print(payload, 2))
    
    -- Leaderboard IDs  octron-mania-<world_id>
    --local world_id = payload.world_id -- FIXME
    -- Only accept world_id matching ^[0-9]+$
    -- TODO
    --local leaderboard_id = "octron-mania-" .. world_id
    
    -- Grab the leaderworld_id from the query
    local leaderboard_id = payload.leaderboard_id
    
    if nil == leaderboard_id then
        error("Leaderboard Id not provided.")
    end
    
    -- string.match() looks like PCRE regex but ain't. Good enough for us, luckily.
    local found_match = string.match(leaderboard_id, '^octron[-]mania[-]%d+$')
    
    if nil == found_match then
        printf("Skipping unrecognized leaderboard `%s'…", leaderboard_id)
        return payload
    end
    
    printf("Looking for leaderboard `%s'…", leaderboard_id)
    
    -- Check if the leaderboard exists
    local sql_query = [[SELECT id FROM leaderboard WHERE id = $1 LIMIT 1]]
    local parameters = {leaderboard_id}
    local rows = nk.sql_query(sql_query, parameters)
    
    -- ~= means !=  (Lua, why!? -- nooo)
    if next(rows) ~= nil then
        printf("Leaderboard `%s' already exists. Done.", leaderboard_id)
        return payload
    end
    
    printf("Trying to create Leaderboard `%s'…", leaderboard_id)
    
    local authoritative = false -- true means only the server may write to it
    local sort = "asc" -- time attack scores ;)
    local operator = "best"
    local reset = "0 0 * * 1" -- Every monday (morning) at midnight
    local metadata = {  -- This is a test, not really used
        --world_id = world_id
    }
    nk.leaderboard_create(leaderboard_id, authoritative, sort, operator, reset, metadata)
    
    printf("Created Leaderboard `%s'…", leaderboard_id)
    
    return payload -- important! This is a hook after all.
end

print("== HOOKING THE LEADERBOARDS CREATION ==")

nk.register_req_before(create_leaderboard_if_inexistent, "ListLeaderboardRecords")
